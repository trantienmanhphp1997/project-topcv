import { IoIosCalculator } from "react-icons/io";
import "./footer.css";
import { FaFile } from "react-icons/fa";
import { IoLocationOutline } from "react-icons/io5";

export default function Footer() {
  return (
    <div className="container">
      <div id="footer-desktop">
        <div className="footer-common-search-keywords">
          <div className="container keyword-seo">
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cv là gì
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Mẫu CV
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              mẫu cv tiếng việt
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Sơ yếu lý lịch
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              CV tham khảo
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              tổng hợp CV tham khảo cho lập trình viên
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              giấy tờ thủ tục hồ sơ xin việc
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Email xin việc bằng tiếng anh
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Mẫu đơn xin việc
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              mẫu đơn xin nghỉ việc
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cách viết đơn xin nghỉ phép
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cách viết CV xin việc
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Ngành Kinh doanh/Bán hàng
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Ngành Kế Toán/Kiểm Toán
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Ngành Nhân Sự{" "}
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV xin Học bổng
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Tiếng Anh
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Tiếng Nhật
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Tiếng Trung
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cách viết CV Tiếng Hàn
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              cẩm nang năm nhất cho sinh viên
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Mẫu đơn xin thực tập
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Hướng dẫn sửa lỗi gõ tiếng Việt
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Ngành du lịch làm việc gì
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cẩm nang xin việc ngành nhân sự
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Xin việc ngành công nghệ thông tin
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cẩm nang xin việc ngành marketing
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cẩm nang xin việc ngành kế toán kiểm toán{" "}
            </a>
            <a target="_blank" href="https://www.topcv.vn/cv-la-gi">
              Cẩm nang xin việc ngành công nghệ thực phẩm
            </a>
          </div>
        </div>
        <div className="footer-main">
          <div className="container">
            <div className="box-main">
              <div className="column">
                <a href="https://www.topcv.vn">
                  <img
                    src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/logo/topcv-logo-footer-6.png"
                    className="img-logo-footer img-responsive"
                    alt="TopCV tuyen dung tai TopCV"
                    title="TopCV tuyển dụng tại TopCV"
                  />
                </a>
                <div className="box-image-flex">
                  <img
                    src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/google_for_startup.png"
                    className="img-responsive"
                    alt=""
                    title=""
                  />
                  <a
                    href="https://www.dmca.com/Protection/Status.aspx?ID=8be40718-7da1-4b43-875a-3efb819100c9&amp;refurl=https://www.topcv.vn/viec-lam"
                    title="DMCA.com Protection Status"
                    className="dmca-badge"
                  >
                    <img
                      src="https://images.dmca.com/Badges/DMCA_badge_grn_60w.png?ID=8be40718-7da1-4b43-875a-3efb819100c9"
                      alt="DMCA.com Protection Status"
                      className="dmca-image"
                    />
                  </a>
                  <a
                    href="http://online.gov.vn/Home/WebDetails/25388"
                    target="_blank"
                  >
                    <img
                      src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/bct.jpg"
                      className="img-responsive bct-image"
                      alt=""
                      title=""
                    />
                  </a>
                </div>
                <div className="box-contact">
                  <p class="title">Liên hệ</p>
                  <span>Hotline:</span>
                  <a href="tel:02466805588">
                    (024) 6680 5588 (Giờ hành chính)
                  </a>{" "}
                  <br />
                  <span>Email:</span>
                  <a href="mailto:hotro@topcv.vn">hotro@topcv.vn</a>
                </div>
                <div className="box-download">
                  <p class="title">Ứng dụng tải xuống</p>
                  <div className="btn-download-app">
                    <a
                      target="_blank"
                      href="https://itunes.apple.com/us/app/topcv-t%E1%BA%A1o-cv-t%C3%ACm-vi%E1%BB%87c-l%C3%A0m/id1455928592?ls=1&amp;mt=8"
                    >
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/download/app_store.png"
                        alt="Tai app TopCV tai App Store"
                        title="Tải app TopCV tại App Store"
                      />
                    </a>
                    <a
                      target="_blank"
                      href="https://play.google.com/store/apps/details?id=com.topcv"
                    >
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/download/chplay.png"
                        alt="Tai app TopCV tai Google Play"
                        title="Tải app TopCV tại Google Play"
                      />
                    </a>
                  </div>
                </div>
                <div className="box-social">
                  <p class="title">Cộng đồng TopCV</p>
                  <div className="btn-list-social">
                    <a
                      target="_blank"
                      href="https://www.facebook.com/topcvbiz/"
                    >
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/facebook.png"
                      />
                    </a>
                    <a
                      target="_blank"
                      href="https://www.youtube.com/c/TopCVpro"
                    >
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/youtube.png"
                      />
                    </a>
                    <a
                      target="_blank"
                      href="https://www.linkedin.com/company/topcv-vietnam"
                    >
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/linkedin.png"
                      />
                    </a>
                    <a target="_blank" href="https://www.tiktok.com/@topcv">
                      <img
                        className="img-responsive"
                        src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/tiktok.png"
                      />
                    </a>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="box-menu-item">
                  <div class="title">Về TopCV</div>
                  <div className="box-menu-child">
                    <a target="_blank" href="https://topcv.com.vn/">
                      Giới thiệu
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#bao-chi"
                    >
                      Góc báo chí
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#lien-he"
                    >
                      Liên hệ
                    </a>
                    <a target="_blank" href="https://www.topcv.vn/faqs">
                      Hỏi đáp
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/dieu-khoan-bao-mat"
                    >
                      Chính sách bảo mật
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/terms-of-service"
                    >
                      Điều khoản dịch vụ
                    </a>
                    <a
                      href="https://static.topcv.vn/manual/Quy_che_san_TMDT_TopCV.pdf"
                      target="_blank"
                    >
                      Quy chế hoạt động
                    </a>
                  </div>
                </div>
                <div className="box-menu-item">
                  <div class="title">Đối tác</div>
                  <div className="box-menu-child">
                    <a href="https://www.testcenter.vn/" target="_blank">
                      TestCenter
                    </a>
                    <a href="https://tophr.vn" target="_blank">
                      TopHR
                    </a>
                    <a href="https://www.viecngay.vn/" target="_blank">
                      ViecNgay
                    </a>
                    <a href="https://happytime.vn/" target="_blank">
                      Happy Time
                    </a>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="box-menu-item">
                  <div class="title">Hồ sơ và CV</div>
                  <div className="box-menu-child">
                    <a target="_blank" href="https://topcv.com.vn/">
                      Quản lý CV của bạn
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#bao-chi"
                    >
                      TopCV Profile
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#lien-he"
                    >
                      Hướng dẫn viết CV
                    </a>
                    <a target="_blank" href="https://www.topcv.vn/faqs">
                      Thư viện CV theo ngành nghề{" "}
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/dieu-khoan-bao-mat"
                    >
                      Review CV
                    </a>
                  </div>
                </div>
                <div className="box-menu-item">
                  <div class="title">Khám phá</div>
                  <div className="box-menu-child">
                    <a href="https://www.testcenter.vn/" target="_blank">
                      Ứng dụng di động TopCV
                    </a>
                    <a href="https://tophr.vn" target="_blank">
                      Tính lương Gross - Net
                    </a>
                    <a href="https://www.viecngay.vn/" target="_blank">
                      Tính lãi suất kép
                    </a>
                    <a href="https://happytime.vn/" target="_blank">
                      Lập kế hoạch tiết kiệm
                    </a>
                    <a href="https://www.testcenter.vn/" target="_blank">
                      Tính bảo hiểm thất nghiệp
                    </a>
                    <a href="https://tophr.vn" target="_blank">
                      Tính bảo hiểm xã hội một lần
                    </a>
                    <a href="https://www.viecngay.vn/" target="_blank">
                      Trắc nghiệm MBTI
                    </a>
                    <a href="https://www.testcenter.vn/" target="_blank">
                      Trắc nghiệm MI
                    </a>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="box-menu-item">
                  <div class="title">Xây dựng sự nghiệp</div>
                  <div className="box-menu-child">
                    <a target="_blank" href="https://topcv.com.vn/">
                      Việc làm tốt nhất
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#bao-chi"
                    >
                      Việc làm lương cao
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/gioi-thieu#lien-he"
                    >
                      Việc làm quản lý
                    </a>
                    <a target="_blank" href="https://www.topcv.vn/faqs">
                      Việc làm IT
                    </a>
                    <a
                      target="_blank"
                      href="https://www.topcv.vn/dieu-khoan-bao-mat"
                    >
                      Việc làm Senior
                    </a>
                    <a
                      href="https://static.topcv.vn/manual/Quy_che_san_TMDT_TopCV.pdf"
                      target="_blank"
                    >
                      Việc làm bán thời gian
                    </a>
                  </div>
                </div>
                <div className="box-menu-item">
                  <div class="title">Phát triển bản thân</div>
                  <div className="box-menu-child">
                    <a href="https://www.testcenter.vn/" target="_blank">
                      TopCV Contest
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="footer-info">
            <div className="container">
              <div className="row" style={{display: 'flex', flexWrap: 'wrap'}}>
                <div className="col-md-10">
                  <h4 className="name-company">
                    Công ty Cổ phần TopCV Việt Nam
                  </h4>
                  <div className="box-tax-code">
                    <div className="group-top">
                      <div className="item">
                        <IoIosCalculator className="calculator" />
                        <span>Giấy phép đăng ký kinh doanh số: </span>
                        <strong>0107307178</strong>
                      </div>
                      <div className="item">
                        <FaFile className="calculator" />
                        <span>Giấy phép hoạt động dịch vụ việc làm số: </span>
                        <strong>18/SLĐTBXH-GP</strong>
                      </div>
                    </div>
                    <div className="item">
                      <IoLocationOutline className="calculator" />
                      <span>Trụ sở HN: </span>
                      <p>
                        Tòa FS - GoldSeason số 47 Nguyễn Tuân, P.Thanh Xuân
                        Trung, Q.Thanh Xuân, Hà Nội
                      </p>
                    </div>
                    <div className="item">
                      <IoLocationOutline className="calculator" />
                      <span>Chi nhánh HCM: </span>
                      <p>
                        Tòa nhà Dali, 24C Phan Đăng Lưu, P.6, Q.Bình Thạnh, TP
                        HCM
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-2 box-qr">
                  <div className="box-qr">
                    <img
                      className="img-responsive"
                      src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/qr_code.png"
                    />
                    <a href="https://topcv.com.vn">topcv.com.vn</a>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 box-ecosystem">
                  <div className="list-ecosystem">
                    <div class="title">Hệ sinh thái HR Tech của TopCV</div>
                    <div className="box-image-app">
                      <a
                        href="https://www.topcv.vn"
                        className="box-image-app__item bg-topcv"
                      >
                        <img
                          src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/topcv-logo.png"
                          className="img-logo-ecosystem img-responsive"
                          alt="TopCV tuyen dung tai TopCV"
                          title="TopCV tuyển dụng tại TopCV"
                        />
                        <span>
                          Nền tảng công nghệ tuyển dụng thông minh TopCV.vn
                        </span>
                      </a>
                      <a
                        href="https://happytime.vn/"
                        className="box-image-app__item bg-happytime"
                      >
                        <img
                          src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/happy_time.png"
                          className="img-logo-ecosystem img-responsive img-padding"
                        />
                        <span>
                          Nền tảng quản lý &amp; gia tăng trải nghiệm nhân viên
                          HappyTime.vn
                        </span>
                      </a>
                      <a
                        href="https://www.testcenter.vn/"
                        className="box-image-app__item bg-testcenter"
                      >
                        <img
                          src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/testcenter.png"
                          className="img-logo-ecosystem img-responsive img-padding"
                        />
                        <span>
                          Nền tảng thiết lập và đánh giá năng lực nhân viên
                          TestCenter.vn
                        </span>
                      </a>
                      <a
                        href="https://www.shiring.ai/"
                        className="box-image-app__item bg-shiring"
                      >
                        <img
                          src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/footer/SHiring.png"
                          className="img-logo-ecosystem img-responsive img-padding"
                        />
                        <span>
                          Giải pháp quản trị tuyển dụng hiệu suất cao SHiring.ai
                        </span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <p className="copyright">
                    © 2014-2024 TopCV Vietnam JSC. All rights reserved.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
