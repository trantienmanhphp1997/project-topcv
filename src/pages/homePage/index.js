import { GrNext, GrPrevious } from "react-icons/gr";
import Footer from "../footer/footer";
import Header from "../header";
import "./homePage.css";
import { Select } from "antd";
import Carousel from "react-multi-carousel";
import { CiHeart } from "react-icons/ci";
import { BsBuildings } from "react-icons/bs";
import { IoLocationOutline } from "react-icons/io5";
import { GoTrash } from "react-icons/go";
import { FaArrowRightLong } from "react-icons/fa6";
import ImagesKinhDoanhPNG from "../images/kinh-doanh-ban-hang.webp";
import ImagesITPhanMem from "../images/it-phan-mem.webp";
import ImagesHanhChinh from "../images/hanh-chinh-van-phong.webp";
import ImagesGiaoDuc from "../images/giao-duc-dao-tao.webp";
import ImagesTuVan from "../images/tu-van.webp";
import ImagesTruyenThong from "../images/marketing-truyen-thong-quang-cao.webp";
import ImagesVanTai from "../images/van-tai-kho-van.webp";
import ImagesKeToan from "../images/ke-toan-kiem-toan.webp";
import { MdOutlineDiamond } from "react-icons/md";
import { FaEnvelope, FaPhoneAlt } from "react-icons/fa";
import { listJob } from '../../mock/jobs';

export default function MasterLayout() {
  return (
    <div>
      <Header />
      <section id="list-feature-jobs">
        <div className="container">
          <section id="box-feature-jobs" className="box_general">
            <div className="box-header">
              <div className="d-flex box-header__wrap">
                <div className="box-header__title">
                  <h2 className="text_ellipsis uppercase box-title">
                    Việc làm tốt nhất
                  </h2>{" "}
                  <div className="box-label">
                    <img
                      src="https://static.topcv.vn/v4/image/welcome/feature-job/label-toppy-ai.png"
                      alt=""
                      style={{
                        width: "140px",
                        height: "35px",
                        borderLeft: "1px solid gray",
                        padding: "5px",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="box-header__tool">
                <span className="see-more">
                  <a
                    href="https://www.topcv.vn/viec-lam-tot-nhat"
                    target="_blank"
                  >
                    Xem tất cả
                  </a>
                </span>
                {/* <span
                  className="btn-feature-jobs-pre btn-slick-arrow slick-arrow"
                  aria-disabled="false"
                  style={{ display: "flex" }}
                >
                  <GrPrevious
                    className="arrow-prev"
                    style={{ color: "20px" }}
                  />
                </span>
                <span
                  className="btn-feature-jobs-next btn-slick-arrow slick-arrow"
                  aria-disabled="false"
                  style={{ display: "flex" }}
                >
                  <GrNext className="arrow-next" style={{ color: "20px" }} />
                </span> */}
              </div>
            </div>
            <div
              className="box-filter"
              style={{ display: "flex", alignItems: "center", flexWrap: 'wrap' }}
            >
              <div className="input">
                <div id="filter-feature-job-container" className="input-group">
                  <Select
                    defaultValue="1"
                    style={{ width: "253px", height: "40px" }}
                  >
                    <Select.Option value="1">Địa điểm</Select.Option>
                    <Select.Option value="2">Mức lương</Select.Option>
                    <Select.Option value="3">Kinh nghiệm</Select.Option>
                    <Select.Option value="4">Ngành nghề</Select.Option>
                  </Select>
                </div>
              </div>
              <div className="box-smart-filter box-smart-feature-jobs">
                <div className="box-smart-location">
                  <div className="">
                    <span
                      className="btn-feature-jobs-pre btn-slick-arrow slick-arrow"
                      aria-disabled="false"
                      style={{ display: "flex" }}
                    >
                      <GrPrevious
                        className="arrow-prev"
                        style={{ color: "20px" }}
                      />
                    </span>
                  </div>
                  <div className="box-smart-list-location">
                    <div data-city_id="1" className="box-smart-item active">
                      Hà Nội
                    </div>
                    <div data-city_id="101" className="box-smart-item">
                      Ba Đình
                    </div>
                    <div data-city_id="102" className="box-smart-item">
                      Hoàn Kiếm
                    </div>
                    <div data-city_id="104" className="box-smart-item">
                      Đống Đa
                    </div>
                    <div data-city_id="105" className="box-smart-item">
                      Tây Hồ
                    </div>
                    <div data-city_id="106" className="box-smart-item">
                      Cầu Giấy
                    </div>
                    <div data-city_id="107" className="box-smart-item">
                      Thanh Xuân
                    </div>
                    <div data-city_id="108" className="box-smart-item">
                      Hoàng Mai
                    </div>
                    <div data-city_id="109" className="box-smart-item">
                      Long Biên
                    </div>
                    <div data-city_id="115" className="box-smart-item">
                      Hà Đông
                    </div>
                    <div data-city_id="130" className="box-smart-item">
                      Bắc Từ Liêm
                    </div>
                    <div data-city_id="131" className="box-smart-item">
                      Nam Từ Liêm
                    </div>
                    <div data-city_id="10000" className="box-smart-item">
                      Các huyện còn lại
                    </div>
                  </div>
                  <div className="">
                    <span
                      className="btn-feature-jobs-pre btn-slick-arrow slick-arrow"
                      aria-disabled="false"
                      style={{ display: "flex" }}
                    >
                      <GrNext
                        className="arrow-next"
                        style={{ color: "20px" }}
                      />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="rectangle">
              <div className="row">
                {
                  listJob?.map((item, index) => (
                    <div className="col-md-4 col-12 mb-3" key={`item${index}`}>
                    <div style={{ display: "flex", backgroundColor: '#ffffff', padding: '8px' }}>
                      <div className="box-company-logo mr-2">
                        <div className="avatar mr-2">
                          <img
                            title="Công ty TNHH Hivelab Vina tuyển dụng tại TopCV"
                            alt="Công ty TNHH Hivelab Vina tuyển dụng tại TopCV"
                            src={item?.linkImgae}
                            data-src={item?.linkImgae}
                            className="img-responsive"
                            style={{ width: "78px", height: "78px" }}
                          />
                        </div>
                      </div>
                      <div className="col-title cvo-flex-grow">
                        <h4>
                          <a
                            href="https://www.topcv.vn/viec-lam/nhan-vien-kinh-doanh-tu-van-sales-tieng-anh-khong-yeu-cau-kinh-nghiem-thu-nhap-10-30-trieu-apo-hair-tai-tp-ha-noi/1324960.html?ta_source=BoxFeatureJob_LinkDetail"
                            data-job-id="1324960"
                            target="_blank"
                            className="title quickview-job text_ellipsis"
                            tabindex="-1"
                          >
                            <b className="job_title">
                            {item.title}
                            </b>
                          </a>
                        </h4>
                        <a
                          href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-komex-viet-nam/153499.html"
                          target="_blank"
                          data-toggle="tooltip"
                          title="CÔNG TY CỔ PHẦN KOMEX VIỆT NAM"
                          data-placement="top"
                          data-container="body"
                          className="text-silver company company_name"
                          tabindex="-1"
                          rel="noreferrer"
                        >
                          {item.company}
                        </a>
                        <div className="box-footer">
                          <div className="col-job-info">
                            <div className="salary">
                              <span className="text_ellipsis">Tới {item.price} USD</span>
                            </div>
                            <div className="salary" style={{ marginLeft: "3px" }}>
                              <span className="text_ellipsis">{item.location}</span>
                            </div>
                            <div className="like" style={{ marginLeft: "85px" }}>
                              <CiHeart style={{ fontSize: "15px" }} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  ))
                }
              </div>
              
            </div>
            <div className="feature-job-page">
              <div className="content">
                <span
                  className="btn-feature-jobs-pre btn-slick-arrow slick-arrow"
                  aria-disabled="false"
                  style={{ display: "flex" }}
                >
                  <GrPrevious
                    className="arrow-prev"
                    style={{ color: "20px" }}
                  />
                </span>
                <div className="feature-job-page__text">
                  <p className="slick-pagination">
                    <span className="hight-light">13</span> / 36 trang
                  </p>
                </div>
                <span
                  className="btn-feature-jobs-pre btn-slick-arrow slick-arrow"
                  aria-disabled="false"
                  style={{ display: "flex" }}
                >
                  <GrNext className="arrow-next" style={{ color: "20px" }} />
                </span>
              </div>
            </div>
          </section>
        </div>
      </section>
      <div
        data-lazy-function="initTopCompany"
        data-ll-status="entered"
        className="top-company lazy entered"
      >
        <div className="container">
          <div className="top-company__header">
            <h2 className="top-company__title">Top Công ty hàng đầu</h2>
          </div>
          <div
            id="slider-company"
            className="top-company__body  slick-initialized slick-slider"
          >
            <div className="slick-list draggable">
              <div className="slick-track">
                <Carousel
                  swipeable={false}
                  draggable={false}
                  showDots={true}
                  responsive={{
                    desktop: {
                      breakpoint: { max: 3000, min: 1024 },
                      items: 3,
                      slidesToSlide: 3, // optional, default to 1.
                    },
                    tablet: {
                      breakpoint: { max: 1024, min: 464 },
                      items: 2,
                      slidesToSlide: 2, // optional, default to 1.
                    },
                    mobile: {
                      breakpoint: { max: 464, min: 0 },
                      items: 1,
                      slidesToSlide: 1, // optional, default to 1.
                    },
                  }}
                  ssr={true} // means to render carousel on server-side.
                  infinite={true}
                  autoPlaySpeed={1000}
                  keyBoardControl={true}
                  customTransition="300ms"
                  transitionDuration={500}
                  containerclassName="carousel-container"
                  removeArrowOnDeviceType={["tablet", "mobile"]}
                  dotListclassName="custom-dot-list-style"
                  itemclassName="carousel-item-padding-40-px"
                >
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/Sản phẩm_ dịch vụ 1.png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/Sản phẩm_ dịch vụ 2.png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/Giá trị.png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/unnamed (10).png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/unnamed (10).png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                  <div
                    className="ccenter-banner-item ta-banner slick-slide slick-current slick-active"
                    style={{ width: "386px" }}
                  >
                    <a
                      href="https://bit.ly/44zcO6A"
                      target="_blank"
                      title="Banner Transcosmos"
                      tabindex="-1"
                    >
                      <img
                        src="https://static.topcv.vn/img/Banner_Center (1).png"
                        alt="Banner Transcosmos"
                        title="Banner Transcosmos"
                        className="img-responsive"
                        style={{
                          width: "370px",
                          height: "207px",
                          borderRadius: "10px",
                        }}
                      />
                    </a>
                  </div>
                </Carousel>
              </div>
            </div>
          </div>
        </div>
        <div id="suggest-job-section" className="suggest-ai">
          <img
            id="arrow"
            src="https://static.topcv.vn/v4/image/job-new/arrow.png"
            alt=""
          />
          <div className="container">
            <div className="top-company__header">
              <h2 className="top-company__title">Gợi ý việc làm phù hợp</h2>
            </div>
            <div className="box-owl-slider">
              <Carousel
                swipeable={false}
                draggable={false}
                responsive={{
                  desktop: {
                    breakpoint: { max: 3000, min: 1024 },
                    items: 3,
                    slidesToSlide: 3, // optional, default to 1.
                  },
                  tablet: {
                    breakpoint: { max: 1024, min: 464 },
                    items: 2,
                    slidesToSlide: 2, // optional, default to 1.
                  },
                  mobile: {
                    breakpoint: { max: 464, min: 0 },
                    items: 1,
                    slidesToSlide: 1, // optional, default to 1.
                  },
                }}
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlaySpeed={1000}
                keyBoardControl={true}
                customTransition="300ms"
                transitionDuration={500}
                containerclassName="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                dotListclassName="custom-dot-list-style"
                itemclassName="carousel-item-padding-40-px"
                showDots={false}
              >
                <div>
                  <div
                    id="job-suggest-list"
                    data-total-item="6"
                    className="body job-suggest-list ta-slick slick-initialized slick-slider"
                    style={{ height: "auto", opacity: "1" }}
                  >
                    <div className="slick-list draggable">
                      <div
                        className="slick-track"
                        style={{
                          opacity: "1",
                          width: "1740px",
                          transform: "translate3d(0px, 0px, 0px)",
                          display: "flex",
                        }}
                      >
                        <div
                          className="slick-slide slick-current slick-active"
                          data-slick-index="0"
                          aria-hidden="false"
                          tabindex="0"
                          style={{ width: "560px" }}
                        >
                          <div>
                            <div
                              id="item_suggest_job_1067414"
                              className="box-job job-ta-slick-suggestion job-ta-suggestion"
                              style={{
                                width: "100%",
                                display: "inline-block",
                                height: "146px",
                              }}
                            >
                              <div className="wrap-item">
                                <div className="box-company-logo">
                                  <div className="avatar">
                                    <a
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      target="_blank"
                                      data-placement="top"
                                      data-container="body"
                                      href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                      tabindex="0"
                                    >
                                      <img
                                        src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/cty-co-phan-blueco-toan-cau-5efeb47db554a.jpg"
                                        alt="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        className="w-100"
                                      />
                                    </a>
                                  </div>
                                </div>
                                <div className="info-job">
                                  <div className="info-job-header">
                                    <h4 className="title">
                                      <a
                                        target="_blank"
                                        href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html?ta_source=JobSuggestInPageJob_LinkDetail&amp;jr_i=elaborate-siege%3A%3A1715908181695-aa3ed5%3A%3Aa3dc454368a94d0e99f763da193b769e%3A%3A1%3A%3A1.0000"
                                        className=""
                                        tabindex="0"
                                      >
                                        <span
                                          data-toggle="tooltip"
                                          data-original-title="Lập Trình Viên Frontend"
                                          data-placement="top"
                                          data-container="body"
                                          className="bold transform-job-title"
                                          aria-describedby="tooltip986160"
                                        >
                                          Lập Trình Viên Frontend
                                        </span>{" "}
                                        <span className="icon-verified-employer level-five"></span>
                                      </a>
                                    </h4>
                                    <div className="salary">
                                      <label> 12 - 20 triệu</label>
                                    </div>
                                  </div>
                                  <a
                                    href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                    className="company"
                                    tabindex="0"
                                  >
                                    <BsBuildings style={{ color: "#6f7882" }} />
                                    <span
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      data-placement="top"
                                      data-container="body"
                                      aria-describedby="tooltip58817"
                                      style={{
                                        fontSize: "13px",
                                        color: "#6f7882",
                                      }}
                                    >
                                      CÔNG TY CỔ PHẦN SAVIS DIGITAL
                                    </span>
                                  </a>
                                  <div className="wrap-update-icon">
                                    <p className="address">
                                      <IoLocationOutline />
                                      <span
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        title="Hà Nội: Cầu Giấy"
                                        aria-describedby="tooltip440670"
                                      >
                                        Hà Nội
                                      </span>
                                    </p>
                                    <div className="icon">
                                      <div
                                        id="box-save-job-1067414"
                                        data-id="1067414"
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-remove-job-recommend"
                                        title="Bỏ qua"
                                        aria-describedby="tooltip322639"
                                      >
                                        <GoTrash />
                                      </div>
                                      <div
                                        id="box-save-job-1067414"
                                        data-toggle="tooltip"
                                        data-type-submit="save"
                                        data-id="1067414"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-save-job unsaved box-save-job-1067414"
                                        title="Lưu"
                                        aria-describedby="tooltip402872"
                                      >
                                        <CiHeart />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div style={{ marginTop: "15px" }}>
                            <div
                              id="item_suggest_job_1067414"
                              className="box-job job-ta-slick-suggestion job-ta-suggestion"
                              style={{
                                width: "100%",
                                display: "inline-block",
                                height: "146px",
                              }}
                            >
                              <div className="wrap-item">
                                <div className="box-company-logo">
                                  <div className="avatar">
                                    <a
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      target="_blank"
                                      data-placement="top"
                                      data-container="body"
                                      href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                      tabindex="0"
                                    >
                                      <img
                                        src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/1vdexJIVO7k9btPofUaMvVzIKyBLm3zz_1709699000____4160582bf234484fe6fa9640e417ee94.jpg"
                                        alt="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        className="w-100"
                                      />
                                    </a>
                                  </div>
                                </div>
                                <div className="info-job">
                                  <div className="info-job-header">
                                    <h4 className="title">
                                      <a
                                        target="_blank"
                                        href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html?ta_source=JobSuggestInPageJob_LinkDetail&amp;jr_i=elaborate-siege%3A%3A1715908181695-aa3ed5%3A%3Aa3dc454368a94d0e99f763da193b769e%3A%3A1%3A%3A1.0000"
                                        className=""
                                        tabindex="0"
                                      >
                                        <span
                                          data-toggle="tooltip"
                                          data-original-title="Lập Trình Viên Frontend"
                                          data-placement="top"
                                          data-container="body"
                                          className="bold transform-job-title"
                                          aria-describedby="tooltip986160"
                                        >
                                          Lập Trình Viên Frontend
                                        </span>{" "}
                                        <span className="icon-verified-employer level-five"></span>
                                      </a>
                                    </h4>
                                    <div className="salary">
                                      <label> 12 - 20 triệu</label>
                                    </div>
                                  </div>
                                  <a
                                    href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                    className="company"
                                    tabindex="0"
                                  >
                                    <BsBuildings style={{ color: "#6f7882" }} />
                                    <span
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      data-placement="top"
                                      data-container="body"
                                      aria-describedby="tooltip58817"
                                      style={{
                                        fontSize: "13px",
                                        color: "#6f7882",
                                      }}
                                    >
                                      CÔNG TY CỔ PHẦN SAVIS DIGITAL
                                    </span>
                                  </a>
                                  <div className="wrap-update-icon">
                                    <p className="address">
                                      <IoLocationOutline />
                                      <span
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        title="Hà Nội: Cầu Giấy"
                                        aria-describedby="tooltip440670"
                                      >
                                        Hà Nội
                                      </span>
                                    </p>
                                    <div className="icon">
                                      <div
                                        id="box-save-job-1067414"
                                        data-id="1067414"
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-remove-job-recommend"
                                        title="Bỏ qua"
                                        aria-describedby="tooltip322639"
                                      >
                                        <GoTrash />
                                      </div>
                                      <div
                                        id="box-save-job-1067414"
                                        data-toggle="tooltip"
                                        data-type-submit="save"
                                        data-id="1067414"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-save-job unsaved box-save-job-1067414"
                                        title="Lưu"
                                        aria-describedby="tooltip402872"
                                      >
                                        <CiHeart />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div
                          className="slick-slide slick-current slick-active"
                          data-slick-index="0"
                          aria-hidden="false"
                          tabindex="0"
                          style={{ width: "560px" }}
                        >
                          <div>
                            <div
                              id="item_suggest_job_1067414"
                              className="box-job job-ta-slick-suggestion job-ta-suggestion"
                              style={{
                                width: "100%",
                                display: "inline-block",
                                height: "146px",
                              }}
                            >
                              <div className="wrap-item">
                                <div className="box-company-logo">
                                  <div className="avatar">
                                    <a
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      target="_blank"
                                      data-placement="top"
                                      data-container="body"
                                      href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                      tabindex="0"
                                    >
                                      <img
                                        src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-an-binh-61d408f161bd6.jpg"
                                        alt="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        className="w-100"
                                      />
                                    </a>
                                  </div>
                                </div>
                                <div className="info-job">
                                  <div className="info-job-header">
                                    <h4 className="title">
                                      <a
                                        target="_blank"
                                        href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html?ta_source=JobSuggestInPageJob_LinkDetail&amp;jr_i=elaborate-siege%3A%3A1715908181695-aa3ed5%3A%3Aa3dc454368a94d0e99f763da193b769e%3A%3A1%3A%3A1.0000"
                                        className=""
                                        tabindex="0"
                                      >
                                        <span
                                          data-toggle="tooltip"
                                          data-original-title="Lập Trình Viên Frontend"
                                          data-placement="top"
                                          data-container="body"
                                          className="bold transform-job-title"
                                          aria-describedby="tooltip986160"
                                        >
                                          Lập Trình Viên Frontend
                                        </span>{" "}
                                        <span className="icon-verified-employer level-five"></span>
                                      </a>
                                    </h4>
                                    <div className="salary">
                                      <label> 12 - 20 triệu</label>
                                    </div>
                                  </div>
                                  <a
                                    href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                    className="company"
                                    tabindex="0"
                                  >
                                    <BsBuildings style={{ color: "#6f7882" }} />
                                    <span
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      data-placement="top"
                                      data-container="body"
                                      aria-describedby="tooltip58817"
                                      style={{
                                        fontSize: "13px",
                                        color: "#6f7882",
                                      }}
                                    >
                                      CÔNG TY CỔ PHẦN SAVIS DIGITAL
                                    </span>
                                  </a>
                                  <div className="wrap-update-icon">
                                    <p className="address">
                                      <IoLocationOutline />
                                      <span
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        title="Hà Nội: Cầu Giấy"
                                        aria-describedby="tooltip440670"
                                      >
                                        Hà Nội
                                      </span>
                                    </p>
                                    <div className="icon">
                                      <div
                                        id="box-save-job-1067414"
                                        data-id="1067414"
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-remove-job-recommend"
                                        title="Bỏ qua"
                                        aria-describedby="tooltip322639"
                                      >
                                        <GoTrash />
                                      </div>
                                      <div
                                        id="box-save-job-1067414"
                                        data-toggle="tooltip"
                                        data-type-submit="save"
                                        data-id="1067414"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-save-job unsaved box-save-job-1067414"
                                        title="Lưu"
                                        aria-describedby="tooltip402872"
                                      >
                                        <CiHeart />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div style={{ marginTop: "15px" }}>
                            <div
                              id="item_suggest_job_1067414"
                              className="box-job job-ta-slick-suggestion job-ta-suggestion"
                              style={{
                                width: "100%",
                                display: "inline-block",
                                height: "146px",
                              }}
                            >
                              <div className="wrap-item">
                                <div className="box-company-logo">
                                  <div className="avatar">
                                    <a
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      target="_blank"
                                      data-placement="top"
                                      data-container="body"
                                      href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                      tabindex="0"
                                    >
                                      <img
                                        src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/FIVj0pdkU90tZ5YZzES6wPWfj7nU4WVQ_1644994914____9ab3541bc46fb2ec28b07b1b8fed172b.png"
                                        alt="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                        className="w-100"
                                      />
                                    </a>
                                  </div>
                                </div>
                                <div className="info-job">
                                  <div className="info-job-header">
                                    <h4 className="title">
                                      <a
                                        target="_blank"
                                        href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html?ta_source=JobSuggestInPageJob_LinkDetail&amp;jr_i=elaborate-siege%3A%3A1715908181695-aa3ed5%3A%3Aa3dc454368a94d0e99f763da193b769e%3A%3A1%3A%3A1.0000"
                                        className=""
                                        tabindex="0"
                                      >
                                        <span
                                          data-toggle="tooltip"
                                          data-original-title="Lập Trình Viên Frontend"
                                          data-placement="top"
                                          data-container="body"
                                          className="bold transform-job-title"
                                          aria-describedby="tooltip986160"
                                        >
                                          Lập Trình Viên Frontend
                                        </span>{" "}
                                        <span className="icon-verified-employer level-five"></span>
                                      </a>
                                    </h4>
                                    <div className="salary">
                                      <label> 12 - 20 triệu</label>
                                    </div>
                                  </div>
                                  <a
                                    href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-savis-digital/116348.html"
                                    className="company"
                                    tabindex="0"
                                  >
                                    <BsBuildings style={{ color: "#6f7882" }} />
                                    <span
                                      data-toggle="tooltip"
                                      data-original-title="CÔNG TY CỔ PHẦN SAVIS DIGITAL"
                                      data-placement="top"
                                      data-container="body"
                                      aria-describedby="tooltip58817"
                                      style={{
                                        fontSize: "13px",
                                        color: "#6f7882",
                                      }}
                                    >
                                      CÔNG TY CỔ PHẦN SAVIS DIGITAL
                                    </span>
                                  </a>
                                  <div className="wrap-update-icon">
                                    <p className="address">
                                      <IoLocationOutline />
                                      <span
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        title="Hà Nội: Cầu Giấy"
                                        aria-describedby="tooltip440670"
                                      >
                                        Hà Nội
                                      </span>
                                    </p>
                                    <div className="icon">
                                      <div
                                        id="box-save-job-1067414"
                                        data-id="1067414"
                                        data-toggle="tooltip"
                                        data-html="true"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-remove-job-recommend"
                                        title="Bỏ qua"
                                        aria-describedby="tooltip322639"
                                      >
                                        <GoTrash />
                                      </div>
                                      <div
                                        id="box-save-job-1067414"
                                        data-toggle="tooltip"
                                        data-type-submit="save"
                                        data-id="1067414"
                                        data-placement="top"
                                        data-container="body"
                                        className="btn-save-job unsaved box-save-job-1067414"
                                        title="Lưu"
                                        aria-describedby="tooltip402872"
                                      >
                                        <CiHeart />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div></div>
                <div></div>
              </Carousel>
            </div>
          </div>
        </div>
      </div>
      <div className="ovf-x-hidden">
        <section
          id="flash-badge-banner"
          className="lazy entered"
          data-lazy-function="initBoxFlashBadge"
          data-ll-status="entered"
        >
          <div className="container">
            <div className="block-content">
              <div className="block-left">
                <h2 className="banner-title">Huy Hiệu Tia Sét</h2>
                <div className="banner-subtitle">
                  Ghi nhận sự tương tác thường xuyên của Nhà tuyển dụng với CV
                  ứng viên
                </div>
                <div
                  id="numberFlashJobs"
                  className="number-flash-jobs"
                  style={{ opacity: "1" }}
                >
                  <span className="value">3.777</span>
                  <span>
                    tin đăng được NTD tương tác thường xuyên trong 24 giờ qua
                  </span>
                </div>
                <div
                  id="boxCountdownUpdate"
                  className="box-countdown-update"
                  style={{ opacity: "1" }}
                >
                  <div className="box-countdown-update-title">
                    Tự động cập nhật sau
                  </div>
                  <div id="boxCountdownTimer" className="box-countdown-clock">
                    <div className="box-countdown-value hour">
                      <span className="value">01</span>
                      <span className="unit">Giờ</span>
                    </div>
                    <div className="dot-divider"></div>
                    <div className="box-countdown-value minute">
                      <span className="value">42</span>
                      <span className="unit">Phút</span>
                    </div>
                    <div className="dot-divider"></div>
                    <div className="box-countdown-value second">
                      <span className="value">29</span>
                      <span className="unit">Giây</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="block-job">
                <div id="pushTopIcon" style={{ display: "none" }}>
                  <img
                    className="entered loaded"
                    data-src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/box-flash-badge/push-top-icon.png"
                    src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/box-flash-badge/push-top-icon.png"
                    data-ll-status="loaded"
                  />
                </div>
                <div id="sliderNewFlashJobPublish" style={{ opacity: "1" }}>
                  <div
                    className="job-item  active"
                    data-index="item-0"
                    data-id="1326832"
                    style={{
                      transform: "scale(1)",
                      opacity: "1",
                      background: "linear-gradient(to right, white, #c8f3db)",
                      borderRadius: "10px",
                    }}
                  >
                    <a
                      className="job-item__logo"
                      target="_blank"
                      href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                    >
                      <img src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/cong-ty-co-phan-tap-doan-dat-nam-605465b4d0cde.jpg" />
                    </a>
                    <div
                      className="tag-job-flash"
                      data-toggle="tooltip-flash-job"
                      data-trigger="manual"
                      data-html="true"
                      data-job-id="1326832"
                      data-template='<div data-job-id=1326832 className="flash-job-tag-tooltip tooltip" role="tooltip"><div className="tooltip-arrow"></div><div className="tooltip-inner"></div></div>'
                      title=""
                      data-placement="top"
                      data-container="body"
                      data-original-title="<div>Tin đăng được NTD tương tác thường xuyên trong 24 giờ qua | <a class='flash-job-tag-tooltip-view-all' href='https://www.topcv.vn/tim-viec-lam-moi-nhat?flash_job=1'>Xem tất cả</a> <i class='fa fa-chevron-right'></i></div>"
                    >
                      <img
                        src="https://www.topcv.vn/v4/image/job-list/icon-flash.webp"
                        alt=""
                      />
                    </div>
                    <div style={{ marginTop: "6px", marginLeft: "6px" }}>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ Bản 8 Triệu) - Hồ Chí Minh"
                        aria-describedby="tooltip696380"
                      >
                        <p className="job-item__title">
                          Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ
                          Bản 8 Triệu) - Hồ Chí Minh
                        </p>
                      </a>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-tap-doan-dat-nam/57944.html"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM"
                      >
                        <p className="job-item__company">
                          CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM
                        </p>
                      </a>
                      <p
                        className="job-item__location"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Hồ Chí Minh"
                      >
                        Hồ Chí Minh
                      </p>
                    </div>
                  </div>
                  <div
                    className="job-item  active"
                    data-index="item-0"
                    data-id="1326832"
                    style={{
                      transform: "scale(1)",
                      opacity: "1",
                      background: "linear-gradient(to right, white, #c8f3db)",
                      borderRadius: "10px",
                    }}
                  >
                    <a
                      className="job-item__logo"
                      target="_blank"
                      href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                    >
                      <img src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/cong-ty-tnhh-hvnet-62c2a500a132d.jpg" />
                    </a>
                    <div
                      className="tag-job-flash"
                      data-toggle="tooltip-flash-job"
                      data-trigger="manual"
                      data-html="true"
                      data-job-id="1326832"
                      data-template='<div data-job-id=1326832 className="flash-job-tag-tooltip tooltip" role="tooltip"><div className="tooltip-arrow"></div><div className="tooltip-inner"></div></div>'
                      title=""
                      data-placement="top"
                      data-container="body"
                      data-original-title="<div>Tin đăng được NTD tương tác thường xuyên trong 24 giờ qua | <a class='flash-job-tag-tooltip-view-all' href='https://www.topcv.vn/tim-viec-lam-moi-nhat?flash_job=1'>Xem tất cả</a> <i class='fa fa-chevron-right'></i></div>"
                    >
                      <img
                        src="https://www.topcv.vn/v4/image/job-list/icon-flash.webp"
                        alt=""
                      />
                    </div>
                    <div style={{ marginTop: "6px", marginLeft: "6px" }}>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ Bản 8 Triệu) - Hồ Chí Minh"
                        aria-describedby="tooltip696380"
                      >
                        <p className="job-item__title">
                          Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ
                          Bản 8 Triệu) - Hồ Chí Minh
                        </p>
                      </a>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-tap-doan-dat-nam/57944.html"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM"
                      >
                        <p className="job-item__company">
                          CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM
                        </p>
                      </a>
                      <p
                        className="job-item__location"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Hồ Chí Minh"
                      >
                        Hồ Chí Minh
                      </p>
                    </div>
                  </div>
                  <div
                    className="job-item  active"
                    data-index="item-0"
                    data-id="1326832"
                    style={{
                      transform: "scale(1)",
                      opacity: "1",
                      background: "linear-gradient(to right, white, #c8f3db)",
                      borderRadius: "10px",
                    }}
                  >
                    <a
                      className="job-item__logo"
                      target="_blank"
                      href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                    >
                      <img src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/5uKU8FVCJnddTjkLjaShF7TrsII6JMLa_1710411708____15cc65b80a4d010e7c571e9e2f9030ef.png" />
                    </a>
                    <div
                      className="tag-job-flash"
                      data-toggle="tooltip-flash-job"
                      data-trigger="manual"
                      data-html="true"
                      data-job-id="1326832"
                      data-template='<div data-job-id=1326832 className="flash-job-tag-tooltip tooltip" role="tooltip"><div className="tooltip-arrow"></div><div className="tooltip-inner"></div></div>'
                      title=""
                      data-placement="top"
                      data-container="body"
                      data-original-title="<div>Tin đăng được NTD tương tác thường xuyên trong 24 giờ qua | <a class='flash-job-tag-tooltip-view-all' href='https://www.topcv.vn/tim-viec-lam-moi-nhat?flash_job=1'>Xem tất cả</a> <i class='fa fa-chevron-right'></i></div>"
                    >
                      <img
                        src="https://www.topcv.vn/v4/image/job-list/icon-flash.webp"
                        alt=""
                      />
                    </div>
                    <div style={{ marginTop: "6px", marginLeft: "6px" }}>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ Bản 8 Triệu) - Hồ Chí Minh"
                        aria-describedby="tooltip696380"
                      >
                        <p className="job-item__title">
                          Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ
                          Bản 8 Triệu) - Hồ Chí Minh
                        </p>
                      </a>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-tap-doan-dat-nam/57944.html"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM"
                      >
                        <p className="job-item__company">
                          CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM
                        </p>
                      </a>
                      <p
                        className="job-item__location"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Hồ Chí Minh"
                      >
                        Hồ Chí Minh
                      </p>
                    </div>
                  </div>
                  <div
                    className="job-item  active"
                    data-index="item-0"
                    data-id="1326832"
                    style={{
                      transform: "scale(1)",
                      opacity: "1",
                      background: "linear-gradient(to right, white, #c8f3db)",
                      borderRadius: "10px",
                    }}
                  >
                    <a
                      className="job-item__logo"
                      target="_blank"
                      href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                    >
                      <img src="https://cdn-new.topcv.vn/unsafe/200x/https://static.topcv.vn/company_logos/P8gIwNgWX9eD7yyfxdbECKxU9M0PCYaT_1708930827____884175130466ea72597fe3434e0e05ac.jpg" />
                    </a>
                    <div
                      className="tag-job-flash"
                      data-toggle="tooltip-flash-job"
                      data-trigger="manual"
                      data-html="true"
                      data-job-id="1326832"
                      data-template='<div data-job-id=1326832 className="flash-job-tag-tooltip tooltip" role="tooltip"><div className="tooltip-arrow"></div><div className="tooltip-inner"></div></div>'
                      title=""
                      data-placement="top"
                      data-container="body"
                      data-original-title="<div>Tin đăng được NTD tương tác thường xuyên trong 24 giờ qua | <a class='flash-job-tag-tooltip-view-all' href='https://www.topcv.vn/tim-viec-lam-moi-nhat?flash_job=1'>Xem tất cả</a> <i class='fa fa-chevron-right'></i></div>"
                    >
                      <img
                        src="https://www.topcv.vn/v4/image/job-list/icon-flash.webp"
                        alt=""
                      />
                    </div>
                    <div style={{ marginTop: "6px", marginLeft: "6px" }}>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/viec-lam/nhan-vien-tu-van-khong-yeu-cau-kinh-nghiem-luong-co-ban-8-trieu-ho-chi-minh/1326832.html?ta_source=FlashSection_LinkDetail"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ Bản 8 Triệu) - Hồ Chí Minh"
                        aria-describedby="tooltip696380"
                      >
                        <p className="job-item__title">
                          Nhân Viên Tư Vấn - Không Yêu Cầu Kinh Nghiệm (Lương Cơ
                          Bản 8 Triệu) - Hồ Chí Minh
                        </p>
                      </a>
                      <a
                        className="job-item__link"
                        target="_blank"
                        href="https://www.topcv.vn/cong-ty/cong-ty-co-phan-tap-doan-dat-nam/57944.html"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM"
                      >
                        <p className="job-item__company">
                          CÔNG TY CỔ PHẦN TẬP ĐOÀN ĐẤT NAM
                        </p>
                      </a>
                      <p
                        className="job-item__location"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Hồ Chí Minh"
                      >
                        Hồ Chí Minh
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="block-intro">
                <div className="block-intro-image">
                  <img
                    className="entered loaded"
                    draggable="false"
                    data-src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/box-flash-badge/flash-badge-intro.png"
                    alt=""
                    srcset=""
                    data-ll-status="loaded"
                    src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/welcome/box-flash-badge/flash-badge-intro.png"
                  />
                </div>
                <div className="block-intro-flashjob-list">
                  <div className="title">
                    Danh sách tin đăng đạt
                    <br />
                    Huy hiệu Tia sét
                  </div>
                  <a
                    href="https://www.topcv.vn/huy-hieu-tia-set"
                    className="btn-watchnow"
                  >
                    Xem Ngay
                    <span>
                      <FaArrowRightLong />
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div className="top-category">
        <div className="container">
          <div className="top-category__header">
            <div className="top-category__box-title">
              <h2 className="top-category__title">Top ngành nghề nổi bật</h2>
            </div>
          </div>
          <div className="top-category__body">
            <div
              id="top-category-carousel"
              className="top-category__carousel owl-carousel owl-theme owl-loaded owl-drag"
            >
              <div className="owl-stage-outer">
                <div
                  className="owl-stage"
                  style={{ transition: "all 0.25s ease 0s" }}
                >
                  <Carousel
                    swipeable={false}
                    draggable={false}
                    responsive={{
                      desktop: {
                        breakpoint: { max: 3000, min: 1024 },
                        items: 3,
                        slidesToSlide: 3, // optional, default to 1.
                      },
                      tablet: {
                        breakpoint: { max: 1024, min: 464 },
                        items: 2,
                        slidesToSlide: 2, // optional, default to 1.
                      },
                      mobile: {
                        breakpoint: { max: 600, min: 0 },
                        items: 1,
                        slidesToSlide: 1, // optional, default to 1.
                      },
                    }}
                    ssr={true} // means to render carousel on server-side.
                    infinite={true}
                    autoPlaySpeed={1000}
                    keyBoardControl={true}
                    customTransition="600ms"
                    transitionDuration={600}
                    containerclassName="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    dotListclassName="custom-dot-list-style"
                    itemclassName="carousel-item-padding-40-px"
                    showDots={false}
                  >
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "1140px", marginRight: "30px" }}
                      >
                        <div className="row mx-2">
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesKinhDoanhPNG}
                                    alt="Kinh doanh / Bán hàng"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Kinh doanh / Bán hàng"
                                  target="_blank"
                                >
                                  Kinh doanh / Bán hàng
                                </a>
                              </div>
                              <p className="top-category__caption">
                                14.647 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesITPhanMem}
                                    alt="IT Phần mềm"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="IT Phần mềm"
                                  target="_blank"
                                >
                                  IT Phần mềm
                                </a>
                              </div>
                              <p className="top-category__caption">
                                3.500 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesHanhChinh}
                                    alt="Hành chính/ Văn phòng"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Hành chính/ Văn phòng"
                                  target="_blank"
                                >
                                  Hành chính/ Văn phòng
                                </a>
                              </div>
                              <p className="top-category__caption">
                                4.500 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesGiaoDuc}
                                    alt="Giáo dục/ Đào tạo"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Giáo dục/ Đào tạo"
                                  target="_blank"
                                >
                                  Giáo dục/ Đào tạo
                                </a>
                              </div>
                              <p className="top-category__caption">
                                3.901 việc làm
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="row mx-2">
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img src={ImagesTuVan} alt="Tư vấn" />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Tư vấn"
                                  target="_blank"
                                >
                                  Tư vấn
                                </a>
                              </div>
                              <p className="top-category__caption">
                                4.679 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesTruyenThong}
                                    alt="Marketting/ Truyền thông/ Quảng cáo"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Marketting/ Truyền thông/ Quảng cáo"
                                  target="_blank"
                                >
                                  Marketting/ Truyền thông/ Quảng cáo
                                </a>
                              </div>
                              <p className="top-category__caption">
                                14.647 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesVanTai}
                                    alt="Vận tải/ Kho vận"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Vận tải/ Kho vận"
                                  target="_blank"
                                >
                                  Vận tải/ Kho vận
                                </a>
                              </div>
                              <p className="top-category__caption">
                                1.390 việc làm
                              </p>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="top-category--item">
                              <div className="top-category__image">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  target="_blank"
                                >
                                  <img
                                    src={ImagesKeToan}
                                    alt="Kế toán/ Kiểm toán"
                                  />
                                </a>
                              </div>
                              <div className="top-category__name">
                                <a
                                  href="/tim-viec-lam-kinh-doanh-ban-hang-c10001"
                                  title="Kế toán/ Kiểm toán"
                                  target="_blank"
                                >
                                  Kế toán/ Kiểm toán
                                </a>
                              </div>
                              <p className="top-category__caption">
                                3.854 việc làm
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div></div>
                    <div></div>
                  </Carousel>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="diamond-employer" className="diamond-employer">
        <div className="container">
          <div>
            <div className="diamond-employer__header">
              <h2 className="diamond-employer__title">
                Nhà tuyển dụng nổi bật
              </h2>
            </div>
            <div
              id="diamond-employer-slider"
              className="diamond-employer__body owl-carousel owl-theme owl-loaded owl-drag"
            >
              <div className="owl-stage-outer">
                <div
                  className="owl-stage"
                  style={{ transition: "all 0.5s ease 0s" }}
                >
                  <Carousel
                    swipeable={false}
                    draggable={false}
                    showDots={true}
                    responsive={{
                      desktop: {
                        breakpoint: { max: 3000, min: 1024 },
                        items: 3,
                        slidesToSlide: 3, // optional, default to 1.
                      },
                      tablet: {
                        breakpoint: { max: 1024, min: 464 },
                        items: 2,
                        slidesToSlide: 2, // optional, default to 1.
                      },
                      mobile: {
                        breakpoint: { max: 464, min: 0 },
                        items: 1,
                        slidesToSlide: 1, // optional, default to 1.
                      },
                    }}
                    ssr={true} // means to render carousel on server-side.
                    infinite={true}
                    autoPlay={true}
                    autoPlaySpeed={2000}
                    keyBoardControl={true}
                    arrows={false}
                    customTransition="300ms"
                    transitionDuration={500}
                    containerclassName="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    dotListclassName="custom-dot-list-style"
                    itemclassName="carousel-item-padding-40-px"
                    sliderclassName="carousel-company"
                  >
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/4309b044c7147d553222bc769da5161d-6194b4d027185.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/4309b044c7147d553222bc769da5161d-6194b4d027185.jpg"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/69iEFNHI0d8edsYdTDgV.jpg"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/edupia-738d242f1fe18d4bbd6e7dc01d9b31c1-6594db35ebfd5.jpg"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/tap-doan-cong-nghiep-vien-thong-quan-doi-6417bb41bf793.jpg"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        className="owl-item active"
                        style={{ width: "212px", marginRight: "20px" }}
                      >
                        <a
                          data-banner-name="banner_c1"
                          href="https://www.topcv.vn/cong-ty/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank/493.html"
                          target="_blank"
                          title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                          className="diamond-employer--box center-banner-item ta-banner"
                        >
                          <span className="icon--top">
                            <MdOutlineDiamond />
                            TOP
                          </span>
                          <img
                            data-src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/ngan-hang-tmcp-viet-nam-thinh-vuong-vpbank-63e1cb5539e62.jpg"
                            src="https://cdn-new.topcv.vn/unsafe/300x/https://static.topcv.vn/company_logos/mUyofsckMXP3yzizOUHPwHfY8D59Yttp_1687234000____832d24e7c60703094b8d0922161d91a7.png"
                            title="Ngân Hàng TMCP Việt Nam Thịnh Vượng (VPBank)"
                            alt="Logo"
                            data-ll-status="loading"
                            className="img-responsive entered loading"
                          />
                        </a>
                      </div>
                    </div>
                  </Carousel>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="hotline">
        <div className="container">
          <div className="hotline-container">
            <h2 className="hotline__title">Hotline Tư Vấn</h2>
            <div className="hotline__tab">
              <ul className="nav">
                <li className="nav__item active">
                  <a className="" data-toggle="tab" href="#candiate">
                    Dành cho Người tìm việc
                  </a>
                </li>
                <li className="nav__item tab-2">
                  <a className="" data-toggle="tab" href="#employer">
                    Dành cho Nhà tuyển dụng
                  </a>
                </li>
              </ul>
              <div className="tab-content">
                <div className="tab-pane active for-candiate" id="candiate">
                  <div className="tab-row">
                    <div className="box-hotline hotline-text">
                      <h2>
                        <span className="first-half">Tìm việc khó</span>
                        <span className="second-half">đã có TopCV</span>
                      </h2>
                      <div className="box-panel">
                        <span className="phone-number">(024) 6680 5588</span>
                        <a href="tel:024 6680 5588" className="btn-call">
                          <FaPhoneAlt />
                          <span className="text-call">GỌI NGAY</span>
                        </a>
                      </div>
                      <div className="box-email">
                        <span className="box-email__title">
                          Email hỗ trợ Ứng viên:
                        </span>
                        <div className="box-email__content">
                          <span className="box-email__content--icon">
                            <FaEnvelope />
                          </span>
                          <a href="mailto:hotro@topcv.vn">hotro@topcv.vn</a>
                        </div>
                      </div>
                    </div>
                    <img
                      className="hotline-image"
                      src="https://cdn-new.topcv.vn/unsafe/https://static.topcv.vn/v4/image/job-new/hotline.png"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="section-box-seo">
        <div className="container">
          <div className="content-seo-box">
            <div id="seo-box">
              <p className="text-seo">
                <strong>
                  Cơ hội ứng tuyển việc làm với đãi ngộ hấp dẫn tại các công ty
                  hàng đầu
                </strong>{" "}
              </p>
              <span className="text-seo normal-text">
                Trước sự phát triển vượt bậc của nền kinh tế, rất nhiều ngành
                nghề trở nên khan hiếm nhân lực hoặc thiếu nhân lực giỏi. Vì
                vậy, hầu hết các trường Đại học đều liên kết với các công ty,
                doanh nghiệp, cơ quan để tạo cơ hội cho các bạn sinh viên được
                học tập, rèn luyện bản thân và làm quen với môi trường làm việc
                từ sớm. Trong{" "}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.topcv.vn/viec-lam"
                >
                  <strong>danh sách việc làm</strong>
                </a>{" "}
                trên đây, TopCV mang đến cho bạn những cơ hội việc làm tại những
                môi trường làm việc năng động, chuyên nghiệp.
              </span>
              <p className="text-seo" style={{ marginTop: "10px" }}>
                <strong>Vậy tại sao nên tìm việc làm tại TopCV?</strong>{" "}
              </p>
              <p className="text-seo">
                <strong>Việc làm Chất lượng</strong>{" "}
              </p>
              <ul>
                {" "}
                <li className="text-seo normal-text">
                  Hàng ngàn tin tuyển dụng chất lượng cao được cập nhật thường
                  xuyên để đáp ứng nhu cầu tìm việc của ứng viên.
                </li>{" "}
                <li className="text-seo normal-text">
                  Hệ thống thông minh tự động gợi ý các công việc phù hợp theo
                  CV của bạn.
                </li>{" "}
              </ul>
              <p className="text-seo">
                <strong>Công cụ viết CV đẹp Miễn phí</strong>{" "}
              </p>
              <ul>
                {" "}
                <li className="text-seo normal-text">
                  Nhiều mẫu CV đẹp, phù hợp nhu cầu ứng tuyển các vị trí khác
                  nhau.
                </li>{" "}
                <li className="text-seo normal-text">
                  Tương tác trực quan, dễ dàng chỉnh sửa thông tin, tạo CV
                  online nhanh chóng trong vòng 5 phút.
                </li>{" "}
              </ul>
              <p className="text-seo">
                <strong>Hỗ trợ Người tìm việc</strong>{" "}
              </p>
              <ul>
                {" "}
                <li className="text-seo normal-text">
                  Nhà tuyển dụng chủ động tìm kiếm và liên hệ với bạn qua hệ
                  thống kết nối ứng viên thông minh.
                </li>{" "}
                <li className="text-seo normal-text">
                  Báo cáo chi tiết Nhà tuyển dụng đã xem CV và gửi offer tới
                  bạn.
                </li>{" "}
              </ul>
              <p className="text-seo normal-text">
                Tại{" "}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.topcv.vn/"
                >
                  <strong>TopCV</strong>
                </a>
                , bạn có thể tìm thấy những tin tuyển dụng việc làm với mức
                lương vô cùng hấp dẫn. Những nhà tuyển dụng kết nối với TopCV
                đều là những công ty lớn tại Việt Nam, nơi bạn có thể làm việc
                trong một môi trường chuyên nghiệp, năng động, trẻ trung. TopCV
                là nền tảng tuyển dụng công nghệ cao giúp các nhà tuyển dụng và
                ứng viên kết nối với nhau. Nhanh tay tạo CV để ứng tuyển vào các
                vị trí việc làm mới nhất hấp dẫn tại{" "}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.topcv.vn/tim-viec-lam-moi-nhat-tai-ha-noi-l1"
                >
                  <strong>việc làm mới nhất tại Hà Nội</strong>
                </a>
                ,{" "}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.topcv.vn/tim-viec-lam-moi-nhat-tai-ho-chi-minh-l2"
                >
                  <strong>việc làm mới nhất tại TP.HCM</strong>
                </a>{" "}
                ở TopCV, bạn sẽ tìm thấy những{" "}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.topcv.vn/tim-viec-lam-moi-nhat"
                >
                  <strong>việc làm mới nhất</strong>
                </a>{" "}
                với mức lương tốt nhất!
              </p>
            </div>
          </div>
          <div className="box-seo-categories">
            <h3 className="title-box" style={{ fontSize: "15px" }}>
              Từ khoá tìm việc làm phổ biến tại TopCV
            </h3>
            <div className="row" style={{marginTop:'20px', display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}} >
              <div className="col-md-4" id="box-find-categories-job">
                <div class="container-box">
                  <h2>Tìm việc làm theo ngành nghề</h2>
                  <ul class="job-list">
                    <li>
                      <a href="#">Việc làm An toàn lao động</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bán hàng kỹ thuật</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bán lẻ / bán sỉ</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Báo chí / Truyền hình</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bảo hiểm</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bảo trì / Sửa chữa</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bất động sản</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Biên / Phiên dịch</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Bưu chính - Viễn thông</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Công nghệ thông tin</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Kế toán</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Marketing</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Nhân sự</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Thiết kế đồ họa</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Quản lý dự án</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Sản xuất</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-4" id="box-find-categories-job">
                <div class="container-box">
                  <h2>Tìm việc làm tại nhà</h2>
                  <ul class="job-list">
                    <li>
                      <a href="#">Việc làm tại Cửu Long</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Đắc Nông</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Điện Biên</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Đồng Tháp</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Gia Lai</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hà Giang</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hà Nam</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hà Tĩnh</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hậu Giang</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hòa Bình</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Khánh Hòa</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Kiên Giang</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hà Nội</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Hồ Chí Minh</a>
                    </li>
                    <li>
                      <a href="#">Việc làm tại Đà Nẵng</a>
                    </li>
                    <li>
                      <a href="#">Việc làm Thái Bình</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-4" id="box-find-categories-job">
                <div class="container-box">
                  <h2>Tìm việc làm phổ biến</h2>
                  <ul class="job-list">
                    <li>
                      <a href="#">Tìm việc làm Thực tập sinh</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Trợ giảng</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Giáo viên</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Thư ký</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Nhân viên kỹ thuật</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm IT Helpdesk</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên PHP</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên Javascript</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên ReactJS</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên VueJS</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên AngularJS</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên NodeJS</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên Java</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình viên .NET</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình Front-End</a>
                    </li>
                    <li>
                      <a href="#">Tìm việc làm Lập trình Back-End</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  );
}
