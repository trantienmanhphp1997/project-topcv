/* eslint-disable react/jsx-no-duplicate-props */
import "./header.css";
import {
  FaArrowAltCircleUp,
  FaBars,
  FaBell,
  FaChartBar,
  FaCheck,
  FaCoins,
  FaCompass,
  FaEye,
  FaFileAlt,
  FaGift,
  FaLaptopCode,
  FaMedal,
  FaRegCalendarCheck,
  FaRegFolder,
  FaSearch,
  FaShoppingBag,
  FaUserCircle,
} from "react-icons/fa";
import { FaMessage, FaPenToSquare, FaSackDollar } from "react-icons/fa6";
import { Button, Carousel, Menu, Popover, Select } from "antd";
import { GrNext, GrPrevious } from "react-icons/gr";
import { icon } from "@fortawesome/fontawesome-svg-core";
import { CiBellOn, CiHeart } from "react-icons/ci";
import { ImProfile } from "react-icons/im";
import { MdOutlineFileUpload, MdOutlineLogout } from "react-icons/md";
import { PiNotePencilBold } from "react-icons/pi";
import { BsBuildings, BsStars } from "react-icons/bs";
import { GoLaw } from "react-icons/go";
import { LuMessagesSquare, LuShieldCheck, LuShieldClose } from "react-icons/lu";
import { HiOutlineDocumentMagnifyingGlass } from "react-icons/hi2";
import { TbPencilDollar } from "react-icons/tb";
import { IoIosBriefcase, IoIosSettings } from "react-icons/io";
import { GiMayanPyramid } from "react-icons/gi";
import { CgMail } from "react-icons/cg";
import { IoShieldCheckmarkOutline } from "react-icons/io5";
import { RiLockPasswordFill } from "react-icons/ri";
import { useState } from "react";

export default function Header() {
  const [showMenu, setShowMenu] = useState(false);
  const items = [
    {
      label: "Việc làm",
      key: "viec_lam",
      children: [
        {
          label: "Tìm việc làm",
          key: "tim_viec_lam",
          icon: <FaSearch style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm đã ứng tuyển",
          key: "viec_lam_ung_tuyen",
          icon: <FaShoppingBag style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm đã lưu",
          key: "viec_lam_da_luu",
          icon: <CiHeart style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm phù hợp",
          key: "viec_lam_phù hợp",
          icon: <FaRegCalendarCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm IT",
          key: "viec_lam_it",
          icon: <FaLaptopCode style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm Senior",
          key: "viec_lam_senior",
          icon: <FaMedal style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      label: "Hồ sơ & CV",
      key: "ho_so",
      children: [
        {
          label: "Quản lý CV",
          key: "quany_ly_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tải CV lên",
          key: "upload_cv",
          icon: <MdOutlineFileUpload style={{ color: "#00b14f" }} />,
        },
        {
          label: "Quản lý Cover Letter",
          key: "quan_ly_letter",
          icon: <FaFileAlt style={{ color: "#00b14f" }} />,
        },
        {
          label: "Mẫu CV",
          key: "mau_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Mẫu Cover Letter",
          key: "mau_cover",
          icon: <FaFileAlt style={{ color: "#00b14f" }} />,
        },
        {
          label: "Dịch vụ tư vấn CV",
          key: "tu_van_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Hướng dẫn viết CV theo ngành nghề",
          key: "huong_dan_viet_cv",
          icon: <PiNotePencilBold style={{ color: "#00b14f" }} />,
        },
        {
          label: "Thư viện CV theo ngành nghề",
          key: "thu_vien_cv",
          icon: <FaRegCalendarCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Top CV Profile",
          key: "top_cv_profile",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      label: "Công ty",
      key: "cong_ty",
      children: [
        {
          label: "Danh sách công ty",
          key: "ds_cong_ty",
          icon: <BsBuildings style={{ color: "#00b14f" }} />,
        },
        {
          label: "Top công ty",
          key: "top_cong_ty",
          icon: <BsStars style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      key: "cong_cu",
      label: "Công cụ",
      children: [
        {
          label: "Tính lương GROSS - NET",
          key: "tinh_luong",
          icon: <GoLaw style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính thuế thu nhập cá nhân",
          key: "tinh_thue",
          icon: <FaChartBar style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính Bảo hiểm thất nghiệp",
          key: "tinh_bao_hiem",
          icon: <LuShieldClose style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính bảo hiểm xã hội một lần",
          key: "tinh_bh_xh",
          icon: <LuShieldCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính lãi suất kép",
          key: "tinh_lai_suat",
          icon: <FaCoins style={{ color: "#00b14f" }} />,
        },
        {
          label: "Lập kế hoạch tiết kiệm",
          key: "lap_ke_hoach",
          icon: <FaSackDollar style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      key: "cam_nang",
      label: "Cẩm nang nghề nghiệp",
      children: [
        {
          label: "Định hướng nghề nghiệp",
          key: "dinh_huong",
          icon: <FaCompass style={{ color: "#00b14f" }} />,
        },
        {
          label: "Bí kíp tìm việc",
          key: "bi_kip_tim_viec",
          icon: (
            <HiOutlineDocumentMagnifyingGlass style={{ color: "#00b14f" }} />
          ),
        },
        {
          label: "Chế độ lương thưởng",
          key: "che_do",
          icon: <TbPencilDollar style={{ color: "#00b14f" }} />,
        },
        {
          label: "Kiến thức chuyên ngành",
          key: "kien_thuc",
          icon: <FaRegFolder style={{ color: "#00b14f" }} />,
        },
        {
          label: "Hành trang nghề nghiệp",
          key: "hanh_trang",
          icon: <IoIosBriefcase style={{ color: "#00b14f" }} />,
        },
        {
          label: "Thị trường và xu hướng tuyển dụng",
          key: "thi_truong",
          icon: <GiMayanPyramid style={{ color: "#00b14f" }} />,
        },
      ],
    },
  ];

  const content = (
    <div className="box-noti">
      <div className="noti-list">
        <div className="noti-item">
          <a
            style={{ color: "black" }}
            href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html"
            target="_blank"
          >
            <div className="notification-item-title">
              <strong>Nhà tuyển dụng vừa xem CV ứng tuyển của bạn</strong>
            </div>
            <div class="notification-item-text">
              Nguyễn Huyền, CÔNG TY CỔ PHẦN SAVIS DIGITAL, Vừa xem CV của bạn
            </div>
            <span className="notification-item-time">10/05/2024</span>
          </a>
        </div>
        <div className="noti-item">
          <a
            style={{ color: "black" }}
            href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html"
            target="_blank"
          >
            <div className="notification-item-title">
              <strong>Nhà tuyển dụng vừa xem CV ứng tuyển của bạn</strong>
            </div>
            <div class="notification-item-text">
              Nguyễn Huyền, CÔNG TY CỔ PHẦN SAVIS DIGITAL, Vừa xem CV của bạn
            </div>
            <span className="notification-item-time">10/05/2024</span>
          </a>
        </div>
        <div className="noti-item">
          <a
            style={{ color: "black" }}
            href="https://www.topcv.vn/viec-lam/lap-trinh-vien-frontend/1067414.html"
            target="_blank"
          >
            <div className="notification-item-title">
              <strong>Nhà tuyển dụng vừa xem CV ứng tuyển của bạn</strong>
            </div>
            <div class="notification-item-text">
              Nguyễn Huyền, CÔNG TY CỔ PHẦN SAVIS DIGITAL, Vừa xem CV của bạn
            </div>
            <span className="notification-item-time">10/05/2024</span>
          </a>
        </div>
      </div>
    </div>
  );
  const contentProfile = (
    <div className="list-profile">
      <div className="item-profile" style={{ alignItems: "center" }}>
        <FaPenToSquare
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Cài đặt thông tin cá nhân</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <FaArrowAltCircleUp
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Nâng cấp tài khoản VIP</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <FaGift
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Kích hoạt quà tặng</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <FaEye
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Nhà tuyển dụng xem hồ sơ</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <IoIosSettings
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Cài đặt gợi ý việc làm</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <CiBellOn
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Cài đặt thông báo việc làm</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <CgMail
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Cài đặt nhận email</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <IoShieldCheckmarkOutline
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Cài đặt bảo mật</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <RiLockPasswordFill
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile">Đổi mật khẩu</span>
      </div>
      <div className="item-profile" style={{ alignItems: "center" }}>
        <MdOutlineLogout
          className="calculator"
          style={{ fontSize: "18px", marginLeft: "10px" }}
        />
        <span className="name-profile" style={{ color: "red" }}>
          Đăng xuất
        </span>
      </div>
    </div>
  );
  const mobileMenu = [
    {
      label: (
        <div
          className="dropdown-menu__info  tag-border"
          style={{
            display: "flex",
            flexWrap: "wrap",
            backgroundColor: "white",
            height: "89px",
          }}
        >
          <img
            src="https://www.topcv.vn/images/avatar-default.jpg"
            style={{
              width: "42px",
              height: "36px",
              borderRadius: "35px",
              marginTop: "37px",
            }}
            onerror="this.src='https://www.topcv.vn/images/avatar-default.jpg'"
            alt="manh tran"
          />
          <div
            className="caption"
            style={{ marginLeft: "14px", fontSize: "13px", marginTop: "-9px" }}
          >
            <p class="name" style={{ color: "#00b14f" }}>
              manh tran
            </p>
            <p style={{ fontWeight: "lighter" }}>
              Mã ứng viên: <b className="code">#8168880</b>
            </p>
            <p class="description" style={{ fontWeight: "lighter" }}>
              trantienmanhphp1997@gmail.com
            </p>
          </div>
        </div>
      ),
      key: "profile",
      children: [
        {
          label: "Cài đặt thông tin cá nhân",
          key: "cai-dat-ttcn",
          icon: <FaPenToSquare style={{ color: "#00b14f" }} />,
        },
        {
          label: "Nâng cấp tài khoản VIP",
          key: "nang-cap-tk",
          icon: <FaArrowAltCircleUp style={{ color: "#00b14f" }} />,
        },
        {
          label: "Kích hoạt quà tặng",
          key: "kich-hoat",
          icon: <FaGift style={{ color: "#00b14f" }} />,
        },
        {
          label: "Nhà tuyển dụng xem hồ sơ",
          key: "nha-tuyen-dung",
          icon: <FaEye style={{ color: "#00b14f" }} />,
        },
        {
          label: "Cài đặt gợi ý việc làm",
          key: "goi-y",
          icon: <IoIosSettings style={{ color: "#00b14f" }} />,
        },
        {
          label: "Cài đặt thông báo việc làm",
          key: "thong-bao",
          icon: <CiBellOn style={{ color: "#00b14f" }} />,
        },
        {
          label: "Cài đặt nhận email",
          key: "email",
          icon: <CgMail style={{ color: "#00b14f" }} />,
        },
        {
          label: "Cài đặt bảo mật",
          key: "bao-mat",
          icon: <IoShieldCheckmarkOutline style={{ color: "#00b14f" }} />,
        },
        {
          label: "Đổi mật khẩu",
          key: "doi-mk",
          icon: <RiLockPasswordFill style={{ color: "#00b14f" }} />,
        },
        {
          label: "Đăng xuất",
          key: "dang-xuat",
          icon: <MdOutlineLogout style={{ color: "red" }} />,
        },
      ],
    },
    {
      label: "Việc làm",
      key: "viec_lam",
      children: [
        {
          label: "Tìm việc làm",
          key: "tim_viec_lam",
          icon: <FaSearch style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm đã ứng tuyển",
          key: "viec_lam_ung_tuyen",
          icon: <FaShoppingBag style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm đã lưu",
          key: "viec_lam_da_luu",
          icon: <CiHeart style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm phù hợp",
          key: "viec_lam_phù hợp",
          icon: <FaRegCalendarCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm IT",
          key: "viec_lam_it",
          icon: <FaLaptopCode style={{ color: "#00b14f" }} />,
        },
        {
          label: "Việc làm Senior",
          key: "viec_lam_senior",
          icon: <FaMedal style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      label: "Hồ sơ & CV",
      key: "ho_so",
      children: [
        {
          label: "Quản lý CV",
          key: "quany_ly_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tải CV lên",
          key: "upload_cv",
          icon: <MdOutlineFileUpload style={{ color: "#00b14f" }} />,
        },
        {
          label: "Quản lý Cover Letter",
          key: "quan_ly_letter",
          icon: <FaFileAlt style={{ color: "#00b14f" }} />,
        },
        {
          label: "Mẫu CV",
          key: "mau_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Mẫu Cover Letter",
          key: "mau_cover",
          icon: <FaFileAlt style={{ color: "#00b14f" }} />,
        },
        {
          label: "Dịch vụ tư vấn CV",
          key: "tu_van_cv",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
        {
          label: "Hướng dẫn viết CV theo ngành nghề",
          key: "huong_dan_viet_cv",
          icon: <PiNotePencilBold style={{ color: "#00b14f" }} />,
        },
        {
          label: "Thư viện CV theo ngành nghề",
          key: "thu_vien_cv",
          icon: <FaRegCalendarCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Top CV Profile",
          key: "top_cv_profile",
          icon: <ImProfile style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      label: "Công ty",
      key: "cong_ty",
      children: [
        {
          label: "Danh sách công ty",
          key: "ds_cong_ty",
          icon: <BsBuildings style={{ color: "#00b14f" }} />,
        },
        {
          label: "Top công ty",
          key: "top_cong_ty",
          icon: <BsStars style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      key: "cong_cu",
      label: "Công cụ",
      children: [
        {
          label: "Tính lương GROSS - NET",
          key: "tinh_luong",
          icon: <GoLaw style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính thuế thu nhập cá nhân",
          key: "tinh_thue",
          icon: <FaChartBar style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính Bảo hiểm thất nghiệp",
          key: "tinh_bao_hiem",
          icon: <LuShieldClose style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính bảo hiểm xã hội một lần",
          key: "tinh_bh_xh",
          icon: <LuShieldCheck style={{ color: "#00b14f" }} />,
        },
        {
          label: "Tính lãi suất kép",
          key: "tinh_lai_suat",
          icon: <FaCoins style={{ color: "#00b14f" }} />,
        },
        {
          label: "Lập kế hoạch tiết kiệm",
          key: "lap_ke_hoach",
          icon: <FaSackDollar style={{ color: "#00b14f" }} />,
        },
      ],
    },
    {
      key: "cam_nang",
      label: "Cẩm nang nghề nghiệp",
      children: [
        {
          label: "Định hướng nghề nghiệp",
          key: "dinh_huong",
          icon: <FaCompass style={{ color: "#00b14f" }} />,
        },
        {
          label: "Bí kíp tìm việc",
          key: "bi_kip_tim_viec",
          icon: (
            <HiOutlineDocumentMagnifyingGlass style={{ color: "#00b14f" }} />
          ),
        },
        {
          label: "Chế độ lương thưởng",
          key: "che_do",
          icon: <TbPencilDollar style={{ color: "#00b14f" }} />,
        },
        {
          label: "Kiến thức chuyên ngành",
          key: "kien_thuc",
          icon: <FaRegFolder style={{ color: "#00b14f" }} />,
        },
        {
          label: "Hành trang nghề nghiệp",
          key: "hanh_trang",
          icon: <IoIosBriefcase style={{ color: "#00b14f" }} />,
        },
        {
          label: "Thị trường và xu hướng tuyển dụng",
          key: "thi_truong",
          icon: <GiMayanPyramid style={{ color: "#00b14f" }} />,
        },
      ],
    },
  ];
  return (
    <>
      <nav className="navbar ta-top">
        <div className="px-30 w-100 d-flex align-item-center">
          <div className="navbar-header" href="https://www.topcv.vn?ref=you">
            <div className="nav-left">
              <a className="navbar-brand">
                <img
                  src="https://static.topcv.vn/v4/image/logo/topcv-logo-6.png"
                  alt="TopCV tuyen dung tai TopCV"
                  title="TopCV tuyển dụng tại TopCV"
                  className="homeImage"
                />
              </a>
              <div className="desktop-menu">
                <Menu
                  mode="horizontal"
                  style={{ height: "60px" }}
                  items={items}
                />
              </div>
            </div>

            <ul className="desktop-menu nav navbar-nav navbar-right">
              <li
                id="nav-notification"
                className="navbar-right__item icon dropdown notification"
              >
                <Popover
                  placement="bottomRight"
                  title={
                    <div
                      className=""
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <strong>Thông báo</strong>
                      <span style={{ color: "#00b14f", fontWeight: "lighter" }}>
                        Đánh dấu là đã đọc
                      </span>
                    </div>
                  }
                  content={content}
                  trigger="click"
                >
                  <a href="javascript:void(0)" className="dropdown-toggle">
                    <FaBell
                      className="noti-icon"
                      style={{ fontSize: "25px", color: "#00b14f" }}
                    />
                    <span className="notification-count">3</span>
                  </a>{" "}
                </Popover>
              </li>
              <li id="topcv-connect" className="navbar-right__item icon">
                <a href="http://candidate.topcvconnect.com" target="_blank">
                  <LuMessagesSquare
                    className="noti-icon"
                    style={{
                      fontSize: "22px",
                      position: "relative",
                      top: "4px",
                    }}
                  />
                </a>
              </li>
              <li className="navbar-right__item group">
                <Popover
                  placement="bottomRight"
                  title={
                    <div
                      className="dropdown-menu__info  tag-border"
                      style={{ display: "flex", flexWrap: "wrap" }}
                    >
                      <img
                        src="https://www.topcv.vn/images/avatar-default.jpg"
                        style={{
                          width: "42px",
                          height: "36px",
                          borderRadius: "35px",
                        }}
                        onerror="this.src='https://www.topcv.vn/images/avatar-default.jpg'"
                        alt="manh tran"
                      />
                      <div className="caption" style={{ marginLeft: "14px" }}>
                        <p class="name" style={{ color: "#00b14f" }}>
                          manh tran
                        </p>
                        <p style={{ fontWeight: "lighter" }}>
                          Mã ứng viên: <b className="code">#8168880</b>
                        </p>
                        <p
                          class="description"
                          style={{ fontWeight: "lighter" }}
                        >
                          trantienmanhphp1997@gmail.com
                        </p>
                      </div>
                    </div>
                  }
                  content={contentProfile}
                  // trigger="click"
                >
                  <a href="javascript:void(0)">
                    <img
                      src="https://www.topcv.vn/images/avatar-default.jpg"
                      onerror="this.src='https://www.topcv.vn/images/avatar-default.jpg'"
                      alt="manh tran"
                      style={{
                        width: "42px",
                        height: "36px",
                        borderRadius: "35px",
                      }}
                    />
                    <span className="fullname" style={{color:'black',fontWeight:'500'}}>manh tran</span>
                  </a>{" "}
                </Popover>
              </li>
            </ul>
          </div>
          {/* <div className="nav navbar-nav navbar-left"> */}

          {/* </div> */}

          <div className="mobile-menu" onClick={() => setShowMenu(!showMenu)}>
            <div className="menu-icon">
              <FaBars className="icon-mobile" />
            </div>
          </div>
        </div>
      </nav>
      {showMenu && (
        <>
          <div className="collapes-menu">
            <Menu
              style={{
                width: "100%",
              }}
              mode="inline"
              items={mobileMenu}
            />
          </div>
        </>
      )}
      <div id="section-header">
        <div class="container">
          <div class="header-box">
            <h1 class="title" style={{ textAlign: "center" }}>
              Tìm việc làm nhanh 24h, việc làm mới nhất trên toàn quốc.
            </h1>
            <p class="description" style={{ fontSize: "16px" }}>
              Tiếp cận <span class="number-hight-light">40,000+</span> tin tuyển
              dụng việc làm mỗi ngày từ hàng nghìn doanh nghiệp uy tín tại Việt
              Nam
            </p>
          </div>
          <form id="frm-search-job" method="GET" onsubmit="return false;">
            <div
              class="group-search"
              style={{ display: "flex", flexWrap: "wrap", width: "100%",justifyContent:'space-between' }}
            >
              <div class="item item-search">
                <i class="fa-light fa-magnifying-glass icon"></i>
                <input
                  class="form-controlui-autocomplete-input"
                  value=""
                  placeholder="Vị trí tuyển dụng"
                  id="keyword"
                  autocomplete="off"
                />
              </div>
              <div class="item search-city">
                <Select
                  defaultValue=""
                  style={{ width: "100%", height: "56px" }}
                >
                  <Select.Option value="">Tất cả tỉnh/thành phố</Select.Option>
                  <Select.Option value="1">Hà Nội</Select.Option>
                  <Select.Option value="2">Hồ Chí Minh</Select.Option>
                  <Select.Option value="3">Bình Dương</Select.Option>
                  <Select.Option value="4">Bắc Ninh</Select.Option>
                  <Select.Option value="5">Đồng Nai</Select.Option>
                  <Select.Option value="6">Hưng Yên</Select.Option>
                  <Select.Option value="7">Hải Dương</Select.Option>
                  <Select.Option value="8">Đà Nẵng</Select.Option>
                  <Select.Option value="9">Hải Phòng</Select.Option>
                  <Select.Option value="10">An Giang</Select.Option>
                  <Select.Option value="11">Bà Rịa-Vũng Tàu</Select.Option>
                  <Select.Option value="12">Bắc Giang</Select.Option>
                  <Select.Option value="13">Bắc Kạn</Select.Option>
                  <Select.Option value="14">Bạc Liêu</Select.Option>
                  <Select.Option value="15">Bến Tre</Select.Option>
                  <Select.Option value="16">Bình Định</Select.Option>
                  <Select.Option value="17">Bình Phước</Select.Option>
                  <Select.Option value="18">Bình Thuận</Select.Option>
                  <Select.Option value="19">Cà Mau</Select.Option>
                  <Select.Option value="20">Cần Thơ</Select.Option>
                  <Select.Option value="21">Cao Bằng</Select.Option>
                  <Select.Option value="22">Cửu Long</Select.Option>
                  <Select.Option value="23">Đắk Lắk</Select.Option>
                  <Select.Option value="24">Đắc Nông</Select.Option>
                  <Select.Option value="25">Điện Biên</Select.Option>
                  <Select.Option value="26">Đồng Tháp</Select.Option>
                  <Select.Option value="27">Gia Lai</Select.Option>
                  <Select.Option value="28">Hà Giang</Select.Option>
                  <Select.Option value="29">Hà Nam</Select.Option>
                  <Select.Option value="30">Hà Tĩnh</Select.Option>
                  <Select.Option value="31">Hậu Giang</Select.Option>
                  <Select.Option value="32">Hoà Bình</Select.Option>
                  <Select.Option value="33">Khánh Hoà</Select.Option>
                  <Select.Option value="34">Kiên Giang</Select.Option>
                  <Select.Option value="35">Kon Tum</Select.Option>
                  <Select.Option value="36">Lai Châu</Select.Option>
                  <Select.Option value="37">Lâm Đồng</Select.Option>
                  <Select.Option value="38">Lạng Sơn</Select.Option>
                  <Select.Option value="39">Lào Cai</Select.Option>
                  <Select.Option value="40">Long An</Select.Option>
                  <Select.Option value="41">Miền Bắc</Select.Option>
                  <Select.Option value="42">Miền Nam</Select.Option>
                  <Select.Option value="43">Miền Trung</Select.Option>
                  <Select.Option value="44">Nam Định</Select.Option>
                  <Select.Option value="45">Nghệ An</Select.Option>
                  <Select.Option value="46">Ninh Bình</Select.Option>
                  <Select.Option value="47">Ninh Thuận</Select.Option>
                  <Select.Option value="48">Phú Thọ</Select.Option>
                  <Select.Option value="49">Phú Yên</Select.Option>
                  <Select.Option value="50">Quảng Bình</Select.Option>
                  <Select.Option value="51">Quảng Nam</Select.Option>
                  <Select.Option value="52">Quảng Ngãi</Select.Option>
                  <Select.Option value="53">Quảng Ninh</Select.Option>
                  <Select.Option value="54">Quảng Trị</Select.Option>
                  <Select.Option value="55">Sóc Trăng</Select.Option>
                  <Select.Option value="56">Sơn La</Select.Option>
                  <Select.Option value="57">Tây Ninh</Select.Option>
                  <Select.Option value="58">Thái Bình</Select.Option>
                  <Select.Option value="59">Thái Nguyên</Select.Option>
                  <Select.Option value="60">Thanh Hoá</Select.Option>
                  <Select.Option value="61">Thừa Thiên Huế</Select.Option>
                  <Select.Option value="62">Tiền Giang</Select.Option>
                  <Select.Option value="63">Toàn Quốc</Select.Option>
                  <Select.Option value="64">Trà Vinh</Select.Option>
                  <Select.Option value="65">Tuyên Quang</Select.Option>
                  <Select.Option value="66">Vĩnh Long</Select.Option>
                  <Select.Option value="67">Vĩnh Phúc</Select.Option>
                  <Select.Option value="68">Yên Bái</Select.Option>
                  <Select.Option value="100">Nước Ngoài</Select.Option>
                </Select>
              </div>
              <div class="item">
                <Select
                  defaultValue=""
                  style={{ width: "100%", height: "56px" }}
                >
                  <Select.Option value="">Chưa có kinh nghiệm</Select.Option>
                  <Select.Option value="2">Dưới 1 năm</Select.Option>
                  <Select.Option value="3">1 năm</Select.Option>
                  <Select.Option value="4">2 năm</Select.Option>
                  <Select.Option value="5">3 năm</Select.Option>
                  <Select.Option value="6">4 năm</Select.Option>
                  <Select.Option value="7">5 năm</Select.Option>
                  <Select.Option value="8">Trên 5 năm</Select.Option>
                </Select>
              </div>
              <div class="item">
                <Select
                  defaultValue=""
                  style={{ width: "100%", height: "56px" }}
                >
                  <Select.Option value="">Tất cả mức lương</Select.Option>
                  <Select.Option value="1">Dưới 10 triệu</Select.Option>
                  <Select.Option value="5">10 - 15 triệu</Select.Option>
                  <Select.Option value="7">15 - 20 triệu</Select.Option>
                  <Select.Option value="8">20 - 25 triệu</Select.Option>
                  <Select.Option value="9">25 - 30 triệu</Select.Option>
                  <Select.Option value="10">30 - 50 triệu</Select.Option>
                  <Select.Option value="11">Trên 50 triệu</Select.Option>
                  <Select.Option value="127">Thoả thuận</Select.Option>
                </Select>
              </div>
            </div>
            <div class="col-button">
              <button class="btn btn-topcv btn-search-job">Tìm kiếm</button>
            </div>
          </form>
          <div class="box-work-market">
            <div class="box-work-market__item">
              <span class="item-label">Vị trí chờ bạn khám phá</span>
              <span
                class="quantity item-number number-job-new-today"
                name="quantity_job_recruitment"
              >
                44.243
              </span>
            </div>
            <div class="box-work-market__item">
              <span class="item-label">Việc làm mới nhất</span>
              <span
                class="quantity item-number number-job-new-today"
                name="quantity_job_new_today"
              >
                2.128
              </span>
            </div>
            <div class="box-work-market__item">
              <span class="item-label">Cập nhật lúc:</span>
              <span class="time-scan item-number number-job-new-today">
                07:38 16/05/2024
              </span>
            </div>
          </div>
        </div>
        <img
          class="img-responsive img-robot img-responsive1"
          src="https://static.topcv.vn/v4/image/job-new/section-header/robot.png"
          alt=""
        />
        <img
          class="img-responsive img-bg-left img-responsive1"
          src="https://static.topcv.vn/v4/image/job-new/section-header/bg-left.png"
          alt=""
        />
        <img
          class="img-responsive img-bg-right img-responsive1"
          src="https://static.topcv.vn/v4/image/job-new/section-header/bg-right.png"
          alt=""
        />
        <img
          class="img-responsive img-bg-left-tablet img-responsive1"
          src="https://static.topcv.vn/v4/image/job-new/section-header/bg-left-tablet.png"
          alt=""
        />
        <img
          class="img-responsive img-bg-right-tablet img-responsive1"
          src="https://static.topcv.vn/v4/image/job-new/section-header/bg-right-tablet.png"
          alt=""
        />
        <div id="section-banner-vip-company">
          <div class="container">
            <div class="banner-vip-companies transition slick-banners slick-initialized slick-slider slick-dotted">
              <div class="slick-list draggable">
                <div class="slick-track">
                  <Carousel arrows infinite={false}>
                    <div>
                      <div
                        data-banner-id="501"
                        data-banner-name="banner_t1"
                        class="banner-1"
                        data-slick-index="-1"
                        aria-hidden="true"
                        tabindex="-1"
                        style={{   }}
                      >
                        <a
                          href="https://bit.ly/3P4so3l"
                          target="_blank"
                          title="Banner Báo cáo thị trường 2024"
                          tabindex="-1"
                        >
                          <img
                            src="https://static.topcv.vn/img/T1 1100x220.png"
                            alt="Banner Báo cáo thị trường 2024"
                            title="Banner Báo cáo thị trường 2024"
                            class="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        data-banner-id="527"
                        data-banner-name="banner_t1"
                        class="banner-1"
                        data-slick-index="0"
                        aria-hidden="true"
                        tabindex="-1"
                        role="tabpanel"
                        id="slick-slide00"
                        aria-describedby="slick-slide-control00"
                        style={{   }}
                      >
                        <a
                          href="https://bit.ly/4b1zCON"
                          target="_blank"
                          title="TEK EXPERTS VIETNAM"
                          tabindex="-1"
                        >
                          <img
                            src="https://static.topcv.vn/img/unnamed (30).png"
                            alt="TEK EXPERTS VIETNAM"
                            title="TEK EXPERTS VIETNAM"
                            class="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        data-banner-id="501"
                        data-banner-name="banner_t1"
                        class="banner-1"
                        data-slick-index="-1"
                        aria-hidden="true"
                        tabindex="-1"
                        style={{   }}
                      >
                        <a
                          href="https://bit.ly/3P4so3l"
                          target="_blank"
                          title="Banner Báo cáo thị trường 2024"
                          tabindex="-1"
                        >
                          <img
                            src="https://static.topcv.vn/img/T1 1100x220.png"
                            alt="Banner Báo cáo thị trường 2024"
                            title="Banner Báo cáo thị trường 2024"
                            class="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                    <div>
                      <div
                        data-banner-id="527"
                        data-banner-name="banner_t1"
                        class="banner-1"
                        data-slick-index="0"
                        aria-hidden="true"
                        tabindex="-1"
                        role="tabpanel"
                        id="slick-slide00"
                        aria-describedby="slick-slide-control00"
                        style={{   }}
                      >
                        <a
                          href="https://bit.ly/4b1zCON"
                          target="_blank"
                          title="TEK EXPERTS VIETNAM"
                          tabindex="-1"
                        >
                          <img
                            src="https://static.topcv.vn/img/unnamed (30).png"
                            alt="TEK EXPERTS VIETNAM"
                            title="TEK EXPERTS VIETNAM"
                            class="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                  </Carousel>
                </div>
              </div>
              <ul
                class="slick-dots"
                style={{ display: "block" }}
                role="tablist"
              >
                <li class="" role="presentation">
                  <button
                    type="button"
                    role="tab"
                    id="slick-slide-control00"
                    aria-controls="slick-slide00"
                    aria-label="1 of 4"
                    tabindex="-1"
                  >
                    1
                  </button>
                </li>
                <li role="presentation" class="">
                  <button
                    type="button"
                    role="tab"
                    id="slick-slide-control01"
                    aria-controls="slick-slide01"
                    aria-label="2 of 4"
                    tabindex="-1"
                  >
                    2
                  </button>
                </li>
                <li role="presentation" class="slick-active">
                  <button
                    type="button"
                    role="tab"
                    id="slick-slide-control02"
                    aria-controls="slick-slide02"
                    aria-label="3 of 4"
                    tabindex="0"
                    aria-selected="true"
                  >
                    3
                  </button>
                </li>
                <li role="presentation" class="">
                  <button
                    type="button"
                    role="tab"
                    id="slick-slide-control03"
                    aria-controls="slick-slide03"
                    aria-label="4 of 4"
                    tabindex="-1"
                  >
                    4
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
